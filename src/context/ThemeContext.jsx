import React from 'react'
import { createTheme, ThemeProvider } from '@material-ui/core/styles'

// Creating custom theme
const theme = createTheme({
    palette: {
        primary: {
            main: 'rgb(0, 171, 85)',
            light: 'rgba(52, 211, 153)',
            dark: 'rgba(6, 95, 70)',
            contrastText: '#fff'
        }
    }
})

// Creating ThemeContext to attach default theme across the application
const ThemeContextProvider = (props) => {
    const { children } = props
    return (
        <ThemeProvider theme={theme}>
            {children}
        </ThemeProvider>
    )
}

export default ThemeContextProvider