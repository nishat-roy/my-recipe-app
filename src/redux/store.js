import { createStore, combineReducers } from 'redux'
import recipeReducer from './reducer'

const rootReducer = combineReducers({
    recipe: recipeReducer
})

const store = createStore(rootReducer)

export default store