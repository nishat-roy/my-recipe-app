const initialState = {
    recipeData: [],
}

export default function recipeReducer(state = initialState, action) {
    switch (action.type) {
        case 'ON_SUBMIT': {
            let recipeData = [...state.recipeData, action.payload]
            localStorage.setItem('recipeData', JSON.stringify(recipeData))
            return { recipeData: [...recipeData] }
        }
        default:
            return state
    }
}