export function submitRecipeData(data) {
    return {
        type: 'ON_SUBMIT',
        payload: data
    }
}