import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        width: '100%',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}))

export default function SelectMenu(props) {
    const classes = useStyles()
    const { menuItems, value, label, handleChange, inpLabel } = props
    return (
        <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="demo-simple-select-outlined-label">{inpLabel}</InputLabel>
            <Select
                fullWidth
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={value}
                onChange={handleChange}
                label={label}
            >
                {menuItems.map((menu, index) => {
                    return (
                        <MenuItem key={index} value={menu}>{menu}</MenuItem>
                    )
                })}
            </Select>
        </FormControl>
    )
}
