import { Fragment } from 'react'
import { TextField } from '@material-ui/core'

export default function Input(props) {

    const {
        type = 'text',
        toggle = () => { },
        onChange = () => { },
        show = false,
        placeholder = '',
        className = '',
        value = '',
        name = '',
        label = '',
        error = {},
        ...rest
    } = props

    return (
        <Fragment>
            <TextField
                autoComplete={'off'}
                className={className}
                name={name}
                type={type}
                value={value}
                onChange={onChange}
                label={label}
                variant="outlined"
                fullWidth
                placeholder={placeholder}
                {...rest}
            />
        </Fragment>
    )
}