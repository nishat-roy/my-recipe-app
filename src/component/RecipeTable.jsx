import { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Paper, TableRow, TableHead, TableContainer, TableCell, TableBody, Table } from '@material-ui/core'
import Input from './shared/Input'
const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
})

// component showing Recipe information inside the table
export default function RecipeTable() {
    const classes = useStyles()
    // Parsing the tableData
    const tableData = JSON.parse(localStorage.getItem('recipeData'))

    // state for recipeData
    const [recipeData, setRecipeData] = useState(tableData)

    const [searchText, setSearchText] = useState('')

    //filter recipe according to search Text
    let filteredRecipe = recipeData.filter((recipe) => recipe.recipeName.includes(searchText))
    return (
        <div style={{ padding: '10px' }}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Input
                        // name='chefName'
                        value={searchText}
                        onChange={(e) => setSearchText(e.target.value)}
                        label='Filter By Name'
                    />
                </Grid>
                <Grid item xs={12}>
                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align='center'>Chef Name</TableCell>
                                    <TableCell align='center'>Images</TableCell>
                                    <TableCell align='center'>Recipe Name</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {filteredRecipe.map((table, index) => (
                                    <TableRow key={index + 12}>
                                        <TableCell component="th" align='center'>
                                            {table.chefName}
                                        </TableCell>
                                        <TableCell align='center'>
                                            {table.imageUrl ? <img height='50px' width='50px' src={table.imageUrl} /> : '-'}
                                        </TableCell>
                                        <TableCell align='center'>{table.recipeName}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </div>
    )
}
