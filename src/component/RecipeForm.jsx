import { useState } from 'react'
import {
    Grid, Paper, Typography, Button
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { useDispatch } from 'react-redux'
import { submitRecipeData } from '../redux/action'
import Input from './shared/Input'
import Select from './shared/Select'

// Initial state
const initialState = {
    chefName: '',
    recipeName: '',
    ingredientInfo: [{
        ingName: '',
        ingQnty: '1',
        ingMeasurement: 'kg',
    }],
    imageUrl: '',
    stepsToCook: ['step 1']
}

const useStyles = makeStyles((theme) => ({
    button: {
        width: '100%',
    },
}))

export default function Recipes() {
    const classes = useStyles()
    const [recipe, setRecipe] = useState(initialState)
    const dispatch = useDispatch()

    // Function for submitted recipes and dispatching to the reducer
    function onSubmit() {
        dispatch(submitRecipeData(recipe))
    }

    /**
     handle changing for input fields
     @e - event fired
     @key - name of the input field
     @index - number (in case of input field inside an array) OR undefined  
     */
    function handleChange(e, key, index = undefined) {
        if (key === 'ingQnty' && Number(e.target.value) <= 0) return
        else if (key === 'stepsToCook' && !isNaN(index)) {
            const cpRecipe = { ...recipe }
            cpRecipe.stepsToCook[index] = e.target.value
            setRecipe(cpRecipe)
        }
        else if (key === 'ingName' || key === 'ingQnty' || key === 'ingMeasurement') {
            setRecipe(prevState => ({
                ...prevState,
                ingredientInfo: prevState.ingredientInfo.map((cookInp, i) => {
                    if (i === index) {
                        cookInp[key] = e.target.value
                    }
                    return { ...cookInp }
                })
            }))
        }
        else {
            setRecipe(prevState => ({ ...prevState, [key]: e.target.value }))
        }
    }

    return (
        <div style={{ padding: '10px' }}>
            <Grid spacing={2} container direction='column' justifyContent='center'>
                <Grid item xs={12}>
                    <Typography variant='h5'>
                        EasierChef
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Input
                        name='chefName'
                        value={recipe.chefName}
                        onChange={(e) => handleChange(e, 'chefName')}
                        label='Chef Name'
                    />
                </Grid>
                <Grid item xs={12}>
                    <Input
                        name='recipeName'
                        value={recipe.recipeName}
                        onChange={(e) => handleChange(e, 'recipeName')}
                        label='Recipe Name'
                    />
                </Grid>
                <Grid item xs={12}>
                    <Grid spacing={2} container direction='column' justifyContent='center'>
                        <Grid item md={12}>
                            <Typography variant='h5'>
                                Ingredients Information
                            </Typography>
                        </Grid>
                        <Grid item md={12} xs={12}>
                            <Paper elevation={3} style={{ padding: '10px' }}>
                                {recipe.ingredientInfo.map((ingInfo, index) => {
                                    return (
                                        <Grid key={index} spacing={2} container direction='row' justifyContent='center' wrap='nowrap' alignItems='center'>
                                            <Grid item md={12} xs={12}>
                                                <Input
                                                    value={ingInfo.ingName}
                                                    onChange={(e) => handleChange(e, 'ingName', index)}
                                                    label='Name'
                                                />
                                            </Grid>
                                            <Grid item md={12} xs={12}>
                                                <Input
                                                    type={'number'}
                                                    name='ingQnty'
                                                    value={ingInfo.ingQnty}
                                                    onChange={(e) => handleChange(e, 'ingQnty', index)}
                                                    label='Quantity'
                                                />
                                            </Grid>
                                            <Grid item md={12} xs={12}>
                                                <Select
                                                    value={ingInfo.ingMeasurement}
                                                    handleChange={(e) => handleChange(e, 'ingMeasurement', index)}
                                                    inpLabel='Measurement'
                                                    menuItems={['kg', 'g']}
                                                />
                                            </Grid>
                                        </Grid>
                                    )
                                })}
                                <Grid item md={12}>
                                    <Grid container align={'center'} justifyContent={'center'}>
                                        <Button
                                            variant='outlined'
                                            color='primary'
                                            onClick={() => {
                                                setRecipe({
                                                    ...recipe, ingredientInfo: [...recipe.ingredientInfo, {
                                                        ingName: '',
                                                        ingQnty: '1',
                                                        ingMeasurement: 'kg',
                                                    }]
                                                })
                                            }}>
                                            Add Ingredients
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={12}>
                    <Input
                        value={recipe.imageUrl}
                        onChange={(e) => handleChange(e, 'imageUrl')}
                        label='Image URL'
                    />
                </Grid>
                <Grid item md={12}>
                    <Grid spacing={2} container direction='column' justifyContent='center'>
                        <Grid item md={12}>
                            <Typography variant='h5'>
                                Steps To Cook
                            </Typography>
                        </Grid>
                        <Grid item md={12}>
                            <Paper elevation={3} style={{ padding: '10px' }}>
                                {recipe.stepsToCook.map((stepInput, index) => {
                                    return (
                                        <Grid key={Math.floor(index / 2)} spacing={2} container direction='row' justifyContent='center' alignItems='center'>
                                            <Grid item xs={3}>
                                                <div>{`Step ${(index + 1)}`}</div>
                                            </Grid>
                                            <Grid item xs={9}>
                                                <Input
                                                    value={stepInput}
                                                    onChange={(e) => handleChange(e, 'stepsToCook', index)}
                                                />
                                            </Grid>
                                        </Grid>
                                    )
                                })}
                                <Grid item md={12}>
                                    <Grid container align={'center'} justifyContent={'center'}>
                                        <Button
                                            style={{ marginTop: '10px' }}
                                            // className={classes.button}
                                            variant='outlined'
                                            color='primary'
                                            onClick={() => { setRecipe({ ...recipe, stepsToCook: [...recipe.stepsToCook, ''] }) }}>
                                            Add Steps
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={12}>
                    <Grid container align={'center'} justifyContent={'center'}>
                        <Button className={classes.button} variant="outlined" color='primary' onClick={onSubmit}>Submit</Button>
                    </Grid>
                </Grid>
            </Grid>
        </div >
    )
}