import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import App from './App'
import ThemeContext from './context/ThemeContext'
import store from './redux/store'
import { BrowserRouter as Router } from 'react-router-dom'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeContext>
        <Router>
          <App />
        </Router>
      </ThemeContext>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
