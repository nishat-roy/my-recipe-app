
import { Link } from 'react-router-dom'
import { Grid, Typography } from '@material-ui/core'
import Index from './component/Index'
import './App.css'
import { renderRoutes, routes } from './routes/index'

function App() {
  return (
    <div>
      <h1 style={{ textAlign: 'center' }}>
        Easier Chef
      </h1>
      <header className='header'>
        <nav className='header_nav'>
          <ul>
            <li><Link to='/'>Recipe Listing</Link></li>
            <li><Link to='/recipe_form'>Recipe Form</Link></li>
          </ul>
        </nav>
      </header>
      {renderRoutes(routes)}
    </div>
  );
}

export default App;
