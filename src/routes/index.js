import { lazy, Suspense, Fragment } from 'react'
import { Switch, Route } from 'react-router-dom'

// Recursive routes rendering
export const renderRoutes = (routes = []) => (
    <Suspense fallback={<div>Page Loading</div>}>
        <Switch>
            {routes.map((route, i) => {
                const Component = route.component
                const Layout = route.layout || Fragment
                return (
                    <Route
                        key={i}
                        path={route.path}
                        exact={route.exact}
                        render={(props) => (
                            <Layout>
                                {route.routes && Array.isArray(route.routes)
                                    ? renderRoutes(route.routes)
                                    : <Component {...props} />}
                            </Layout>
                        )}
                    />
                );
            })}
        </Switch>
    </Suspense>
)



// Routes array containing each objects that corresponds to routes information  
export const routes = [
    {
        name: 'Recipe Table',
        exact: true,
        path: ['/'],
        component: lazy(() => import('../component/RecipeTable'))
    },
    {
        name: 'Recipe Form',
        exact: true,
        path: ['/recipe_form'],
        component: lazy(() => import('../component/RecipeForm'))
    },
]